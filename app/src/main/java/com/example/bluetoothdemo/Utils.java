package com.example.bluetoothdemo;

import android.bluetooth.BluetoothDevice;

import java.lang.reflect.Method;

public class Utils {

    public static void bond(BluetoothDevice device){
        try {
            Method method = BluetoothDevice.class.getMethod("createBond");
            method.invoke(device);
        }catch (Exception exception){

        }
    }

    public static void removeBonds(BluetoothDevice device){
        try {
            Method method = BluetoothDevice.class.getMethod("removeBond");
            method.invoke(device);
        }catch (Exception exception){

        }
    }

}
